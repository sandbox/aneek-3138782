<?php

namespace Drupal\ip2country_event\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\ip2country_event\Ip2CountryStorageServiceInterface;

/**
 * Event subscriber for the Request event to track the IP based Country.
 */
class Ip2CountrySubscriber implements EventSubscriberInterface {

  /**
   * This module's IP/Country Retrieve or storage service instance.
   *
   * @var \Drupal\ip2country_event\Ip2CountryStorageServiceInterface
   */
  protected $ipCountryStorage;

  /**
   * Constructs EventSubscriber with proper arguments.
   *
   * @param \Drupal\ip2country_event\Ip2CountryStorageServiceInterface $storage
   *   Ip2Country lookup instance.
   */
  public function __construct(Ip2CountryStorageServiceInterface $storage) {
    $this->ipCountryStorage = $storage;
  }

  /**
   * The subscriber method to get the Country code & name to set in Request.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Symfony Get Response event.
   */
  public function onRequest(GetResponseEvent $event) {
    $request = $event->getRequest();
    $ip = $request->getClientIp();
    $ip = "109.89.246.61";
    $a = $this->ipStorage->getCountryCode($ip);
    $b = $this->ipStorage->getCountryName($a);
    echo $b;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onRequest',
    ];
  }

}
