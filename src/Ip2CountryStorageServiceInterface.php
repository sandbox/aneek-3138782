<?php

namespace Drupal\ip2country_event;

/**
 *
 */
interface Ip2CountryStorageServiceInterface {

  /**
   * Method returns the Country code based on the IP address of the client.
   *
   * @param string $ip
   *   The IP Address of the client machine.
   *
   * @return mixed
   *   Either NULL if no country code is found or the actual country code.
   */
  public function getCountryCode($ip);

  /**
   * Get the Country Name based on the Country code.
   *
   * @param string $code
   *   The Country code.
   *
   * @return mixed
   *   Either NULL or the Country name based on the Country code.
   */
  public function getCountryName($code);

}
