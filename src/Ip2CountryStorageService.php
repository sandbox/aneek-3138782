<?php

namespace Drupal\ip2country_event;

use Drupal\ip2country\Ip2CountryLookupInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

class Ip2CountryStorageService implements Ip2CountryStorageServiceInterface {

  /**
   * Ip2Country lookup instance.
   *
   * @var \Drupal\ip2country\Ip2CountryLookupInterface
   */
  protected $lookup;

  /**
   * Locale country detection interface.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * The cache backend instance.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs EventSubscriber with proper arguments.
   *
   * @param \Drupal\ip2country\Ip2CountryLookupInterface $lookup
   *   Ip2Country lookup instance.
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   Locale country manager interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $ip_cache
   *   The Cache backend interface.
   */
  public function __construct(
    Ip2CountryLookupInterface $lookup,
    CountryManagerInterface $country_manager,
    CacheBackendInterface $ip_cache
  ) {
    $this->lookup = $lookup;
    $this->countryManager = $country_manager;
    $this->cacheBackend = $ip_cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getCountryCode($ip) {
    return $this->lookup->getCountry($ip);
  }

  /**
   * {@inheritDoc}
   */
  public function getCountryName($code) {
    $countries = $this->countryManager->getList();
    if (isset($countries[$code]) && !empty($countries[$code])) {
      return $countries[$code];
    }
    return NULL;
  }

}
